﻿namespace SN_send_Receive_to_HX
{
    partial class Form_SNwriteRead
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_SNwriteRead));
            this.button_Send = new System.Windows.Forms.Button();
            this.button_ReceiveSN = new System.Windows.Forms.Button();
            this.serialPort = new System.IO.Ports.SerialPort(this.components);
            this.gbPortSettings = new System.Windows.Forms.GroupBox();
            this.button_ReScanComPort = new System.Windows.Forms.Button();
            this.checkBox_ComportOpen = new System.Windows.Forms.CheckBox();
            this.cmbPortName = new System.Windows.Forms.ComboBox();
            this.cmbBaudRate = new System.Windows.Forms.ComboBox();
            this.cmbStopBits = new System.Windows.Forms.ComboBox();
            this.cmbParity = new System.Windows.Forms.ComboBox();
            this.cmbDataBits = new System.Windows.Forms.ComboBox();
            this.lblComPort = new System.Windows.Forms.Label();
            this.lblStopBits = new System.Windows.Forms.Label();
            this.lblBaudRate = new System.Windows.Forms.Label();
            this.lblDataBits = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button_Clear = new System.Windows.Forms.Button();
            this.textBox_SerialPort = new System.Windows.Forms.RichTextBox();
            this.textBox_Send = new System.Windows.Forms.RichTextBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.richTextBox_ReceiveSN = new System.Windows.Forms.RichTextBox();
            this.button_ClearRx = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.gbPortSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // button_Send
            // 
            this.button_Send.Location = new System.Drawing.Point(640, 161);
            this.button_Send.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button_Send.Name = "button_Send";
            this.button_Send.Size = new System.Drawing.Size(175, 39);
            this.button_Send.TabIndex = 1;
            this.button_Send.Text = "Send";
            this.button_Send.UseVisualStyleBackColor = true;
            this.button_Send.Click += new System.EventHandler(this.button_Send_Click);
            // 
            // button_ReceiveSN
            // 
            this.button_ReceiveSN.Location = new System.Drawing.Point(640, 266);
            this.button_ReceiveSN.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button_ReceiveSN.Name = "button_ReceiveSN";
            this.button_ReceiveSN.Size = new System.Drawing.Size(175, 34);
            this.button_ReceiveSN.TabIndex = 2;
            this.button_ReceiveSN.Text = "Receive SN";
            this.button_ReceiveSN.UseVisualStyleBackColor = true;
            this.button_ReceiveSN.Click += new System.EventHandler(this.button_ReceiveSN_Click);
            // 
            // gbPortSettings
            // 
            this.gbPortSettings.Controls.Add(this.button_ReScanComPort);
            this.gbPortSettings.Controls.Add(this.checkBox_ComportOpen);
            this.gbPortSettings.Controls.Add(this.cmbPortName);
            this.gbPortSettings.Controls.Add(this.cmbBaudRate);
            this.gbPortSettings.Controls.Add(this.cmbStopBits);
            this.gbPortSettings.Controls.Add(this.cmbParity);
            this.gbPortSettings.Controls.Add(this.cmbDataBits);
            this.gbPortSettings.Controls.Add(this.lblComPort);
            this.gbPortSettings.Controls.Add(this.lblStopBits);
            this.gbPortSettings.Controls.Add(this.lblBaudRate);
            this.gbPortSettings.Controls.Add(this.lblDataBits);
            this.gbPortSettings.Controls.Add(this.label3);
            this.gbPortSettings.Location = new System.Drawing.Point(15, 18);
            this.gbPortSettings.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gbPortSettings.Name = "gbPortSettings";
            this.gbPortSettings.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gbPortSettings.Size = new System.Drawing.Size(794, 85);
            this.gbPortSettings.TabIndex = 11;
            this.gbPortSettings.TabStop = false;
            this.gbPortSettings.Text = "COM Serial Port Settings";
            // 
            // button_ReScanComPort
            // 
            this.button_ReScanComPort.AutoSize = true;
            this.button_ReScanComPort.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_ReScanComPort.Location = new System.Drawing.Point(548, 37);
            this.button_ReScanComPort.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button_ReScanComPort.Name = "button_ReScanComPort";
            this.button_ReScanComPort.Size = new System.Drawing.Size(103, 31);
            this.button_ReScanComPort.TabIndex = 10;
            this.button_ReScanComPort.Text = "ReScan Ports";
            this.button_ReScanComPort.UseVisualStyleBackColor = true;
            this.button_ReScanComPort.Click += new System.EventHandler(this.button_ReScanComPort_Click);
            // 
            // checkBox_ComportOpen
            // 
            this.checkBox_ComportOpen.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBox_ComportOpen.AutoSize = true;
            this.checkBox_ComportOpen.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_ComportOpen.Location = new System.Drawing.Point(657, 37);
            this.checkBox_ComportOpen.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.checkBox_ComportOpen.Name = "checkBox_ComportOpen";
            this.checkBox_ComportOpen.Size = new System.Drawing.Size(84, 29);
            this.checkBox_ComportOpen.TabIndex = 8;
            this.checkBox_ComportOpen.Text = "Open Port";
            this.checkBox_ComportOpen.UseVisualStyleBackColor = true;
            this.checkBox_ComportOpen.CheckedChanged += new System.EventHandler(this.checkBox_ComportOpen_CheckedChanged);
            // 
            // cmbPortName
            // 
            this.cmbPortName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPortName.FormattingEnabled = true;
            this.cmbPortName.Items.AddRange(new object[] {
            "COM1",
            "COM2",
            "COM3",
            "COM4",
            "COM5",
            "COM6"});
            this.cmbPortName.Location = new System.Drawing.Point(9, 42);
            this.cmbPortName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbPortName.Name = "cmbPortName";
            this.cmbPortName.Size = new System.Drawing.Size(87, 27);
            this.cmbPortName.TabIndex = 1;
            this.cmbPortName.Tag = "1";
            // 
            // cmbBaudRate
            // 
            this.cmbBaudRate.FormattingEnabled = true;
            this.cmbBaudRate.Items.AddRange(new object[] {
            "1200",
            "2400",
            "4800",
            "9600",
            "19200",
            "38400",
            "57600",
            "115200",
            "960000"});
            this.cmbBaudRate.Location = new System.Drawing.Point(106, 42);
            this.cmbBaudRate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbBaudRate.Name = "cmbBaudRate";
            this.cmbBaudRate.Size = new System.Drawing.Size(117, 27);
            this.cmbBaudRate.TabIndex = 3;
            this.cmbBaudRate.Text = "960000";
            // 
            // cmbStopBits
            // 
            this.cmbStopBits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStopBits.FormattingEnabled = true;
            this.cmbStopBits.Items.AddRange(new object[] {
            "1",
            "2",
            "3"});
            this.cmbStopBits.Location = new System.Drawing.Point(409, 41);
            this.cmbStopBits.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbStopBits.Name = "cmbStopBits";
            this.cmbStopBits.Size = new System.Drawing.Size(116, 27);
            this.cmbStopBits.TabIndex = 9;
            // 
            // cmbParity
            // 
            this.cmbParity.DisplayMember = "1";
            this.cmbParity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbParity.FormattingEnabled = true;
            this.cmbParity.Items.AddRange(new object[] {
            "None",
            "Even",
            "Odd"});
            this.cmbParity.Location = new System.Drawing.Point(233, 41);
            this.cmbParity.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbParity.Name = "cmbParity";
            this.cmbParity.Size = new System.Drawing.Size(78, 27);
            this.cmbParity.TabIndex = 5;
            this.cmbParity.Tag = "1";
            // 
            // cmbDataBits
            // 
            this.cmbDataBits.FormattingEnabled = true;
            this.cmbDataBits.Items.AddRange(new object[] {
            "5",
            "6",
            "7",
            "8"});
            this.cmbDataBits.Location = new System.Drawing.Point(320, 41);
            this.cmbDataBits.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbDataBits.Name = "cmbDataBits";
            this.cmbDataBits.Size = new System.Drawing.Size(78, 27);
            this.cmbDataBits.TabIndex = 7;
            this.cmbDataBits.Text = "8";
            // 
            // lblComPort
            // 
            this.lblComPort.AutoSize = true;
            this.lblComPort.Location = new System.Drawing.Point(8, 19);
            this.lblComPort.Name = "lblComPort";
            this.lblComPort.Size = new System.Drawing.Size(76, 19);
            this.lblComPort.TabIndex = 0;
            this.lblComPort.Text = "COM Port:";
            // 
            // lblStopBits
            // 
            this.lblStopBits.AutoSize = true;
            this.lblStopBits.Location = new System.Drawing.Point(411, 18);
            this.lblStopBits.Name = "lblStopBits";
            this.lblStopBits.Size = new System.Drawing.Size(70, 19);
            this.lblStopBits.TabIndex = 8;
            this.lblStopBits.Text = "Stop Bits:";
            // 
            // lblBaudRate
            // 
            this.lblBaudRate.AutoSize = true;
            this.lblBaudRate.Location = new System.Drawing.Point(105, 19);
            this.lblBaudRate.Name = "lblBaudRate";
            this.lblBaudRate.Size = new System.Drawing.Size(80, 19);
            this.lblBaudRate.TabIndex = 2;
            this.lblBaudRate.Text = "Baud Rate:";
            // 
            // lblDataBits
            // 
            this.lblDataBits.AutoSize = true;
            this.lblDataBits.Location = new System.Drawing.Point(323, 18);
            this.lblDataBits.Name = "lblDataBits";
            this.lblDataBits.Size = new System.Drawing.Size(73, 19);
            this.lblDataBits.TabIndex = 6;
            this.lblDataBits.Text = "Data Bits:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(235, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 19);
            this.label3.TabIndex = 4;
            this.label3.Text = "Parity:";
            // 
            // button_Clear
            // 
            this.button_Clear.Location = new System.Drawing.Point(640, 779);
            this.button_Clear.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button_Clear.Name = "button_Clear";
            this.button_Clear.Size = new System.Drawing.Size(175, 34);
            this.button_Clear.TabIndex = 13;
            this.button_Clear.Text = "Clear";
            this.button_Clear.UseVisualStyleBackColor = true;
            this.button_Clear.Click += new System.EventHandler(this.button_Clear_Click);
            // 
            // textBox_SerialPort
            // 
            this.textBox_SerialPort.Location = new System.Drawing.Point(823, 18);
            this.textBox_SerialPort.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBox_SerialPort.Name = "textBox_SerialPort";
            this.textBox_SerialPort.Size = new System.Drawing.Size(854, 793);
            this.textBox_SerialPort.TabIndex = 14;
            this.textBox_SerialPort.Text = "";
            this.textBox_SerialPort.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_SerialPort_KeyDown);
            this.textBox_SerialPort.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_SerialPort_KeyPress);
            // 
            // textBox_Send
            // 
            this.textBox_Send.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_Send.Location = new System.Drawing.Point(10, 130);
            this.textBox_Send.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBox_Send.MaxLength = 26;
            this.textBox_Send.Name = "textBox_Send";
            this.textBox_Send.Size = new System.Drawing.Size(614, 70);
            this.textBox_Send.TabIndex = 15;
            this.textBox_Send.Text = "";
            this.textBox_Send.TextChanged += new System.EventHandler(this.textBox_Send_TextChanged_1);
            this.textBox_Send.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_Send_KeyDown);
            this.textBox_Send.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_Send_KeyPress);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 107);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 19);
            this.label1.TabIndex = 19;
            this.label1.Text = "SN to Send";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // richTextBox_ReceiveSN
            // 
            this.richTextBox_ReceiveSN.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox_ReceiveSN.Location = new System.Drawing.Point(10, 256);
            this.richTextBox_ReceiveSN.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.richTextBox_ReceiveSN.MaxLength = 20;
            this.richTextBox_ReceiveSN.Name = "richTextBox_ReceiveSN";
            this.richTextBox_ReceiveSN.ReadOnly = true;
            this.richTextBox_ReceiveSN.Size = new System.Drawing.Size(614, 84);
            this.richTextBox_ReceiveSN.TabIndex = 20;
            this.richTextBox_ReceiveSN.Text = "";
            // 
            // button_ClearRx
            // 
            this.button_ClearRx.Location = new System.Drawing.Point(10, 348);
            this.button_ClearRx.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button_ClearRx.Name = "button_ClearRx";
            this.button_ClearRx.Size = new System.Drawing.Size(175, 34);
            this.button_ClearRx.TabIndex = 21;
            this.button_ClearRx.Text = "Clear Rx Tx";
            this.button_ClearRx.UseVisualStyleBackColor = true;
            this.button_ClearRx.Click += new System.EventHandler(this.button_ClearRx_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::SN_send_Receive_to_HX.Properties.Resources.download1;
            this.pictureBox1.Location = new System.Drawing.Point(10, 696);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(256, 122);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 22;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 233);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 19);
            this.label2.TabIndex = 24;
            this.label2.Text = "SN received";
            // 
            // Form_SNwriteRead
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1695, 830);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button_ClearRx);
            this.Controls.Add(this.richTextBox_ReceiveSN);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox_Send);
            this.Controls.Add(this.textBox_SerialPort);
            this.Controls.Add(this.button_Clear);
            this.Controls.Add(this.gbPortSettings);
            this.Controls.Add(this.button_ReceiveSN);
            this.Controls.Add(this.button_Send);
            this.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Form_SNwriteRead";
            this.Text = "SN write Read";
            this.Load += new System.EventHandler(this.Form_SNwriteRead_Load);
            this.gbPortSettings.ResumeLayout(false);
            this.gbPortSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button_Send;
        private System.Windows.Forms.Button button_ReceiveSN;
        private System.IO.Ports.SerialPort serialPort;
        private System.Windows.Forms.GroupBox gbPortSettings;
        private System.Windows.Forms.Button button_ReScanComPort;
        private System.Windows.Forms.CheckBox checkBox_ComportOpen;
        private System.Windows.Forms.ComboBox cmbPortName;
        private System.Windows.Forms.ComboBox cmbBaudRate;
        private System.Windows.Forms.ComboBox cmbStopBits;
        private System.Windows.Forms.ComboBox cmbParity;
        private System.Windows.Forms.ComboBox cmbDataBits;
        private System.Windows.Forms.Label lblComPort;
        private System.Windows.Forms.Label lblStopBits;
        private System.Windows.Forms.Label lblBaudRate;
        private System.Windows.Forms.Label lblDataBits;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button_Clear;
        private System.Windows.Forms.RichTextBox textBox_SerialPort;
        private System.Windows.Forms.RichTextBox textBox_Send;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox richTextBox_ReceiveSN;
        private System.Windows.Forms.Button button_ClearRx;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label2;
    }
}

