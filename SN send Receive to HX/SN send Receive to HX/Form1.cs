﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Threading;


namespace SN_send_Receive_to_HX
{
    public partial class Form_SNwriteRead : Form
    {

        public Form_SNwriteRead()
        {
            InitializeComponent();
        }

        private void textBox_Send_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form_SNwriteRead_Load(object sender, EventArgs e)
        {
            serialPort.DataReceived += SerialPort1_DataReceived;

            scanComports();


            cmbStopBits.DataSource = Enum.GetValues(typeof(StopBits));
            cmbStopBits.SelectedIndex = (int)StopBits.One;

            cmbParity.DataSource = Enum.GetValues(typeof(Parity));
            cmbParity.SelectedIndex = (int)Parity.None;

        }

        private const int SN_SIZE = 26;
        string IncomingString = "";
        private void ParseIncomingBuffer(byte[] i_buffer)
        {
            try
            {


                IncomingString += "\n\n------------------------------------";
                    foreach (byte by in i_buffer)
                    {
                        IncomingString += by.ToString("X2") + " ";
                    }

                 //   textBox_SerialPort_addText(IncomingString);

                    //Get opCode
                    short Opcode = BitConverter.ToInt16(i_buffer, 3);

                    textBox_SerialPort_addText("Opcode Received = [0x" + Opcode.ToString("X4") + "]");

                    switch(Opcode)
                    {
                        case 0x0A01:
                            byte[] StringBufferbuffer = new byte[i_buffer.Length];
                            int j = 0;
                            for (int i=13; i < (i_buffer.Length - 3); i++)
                            {
                                StringBufferbuffer[j] = i_buffer[i];
                                j++;
                            }

                            var str = System.Text.Encoding.Default.GetString(StringBufferbuffer);

                        if (str.StartsWith("Serial Number:") == true)
                        {
                            SN_Received = true;
                            String SN_Number2 = str.Substring(15,8);
                            
                            
                            richTextBox_ReceiveSN.BeginInvoke(new EventHandler(delegate
                            {
                                richTextBox_ReceiveSN.Text = SN_Number2;
                                if (SN_Number2 == textBox_Send.Text)
                                {
                                    richTextBox_ReceiveSN.BackColor = Color.LightGreen;
                                }
                            }));
                        }
                        

                            textBox_SerialPort_addText("Incoming string = " + str);
                            break;

                        case 0x0ABE:
                        //textBox_SerialPort_addText("Incoming string = " );
                        SN_Received = true;
                        byte[] SN_Number = new byte[SN_SIZE];
                        for(int i=0; i < SN_SIZE; i++)
                        {
                            SN_Number[i] = i_buffer[i + 13];
                        }

                        String Str_SN_Number = System.Text.Encoding.Default.GetString(SN_Number);


                        richTextBox_ReceiveSN.BeginInvoke(new EventHandler(delegate
                        {
                            richTextBox_ReceiveSN.Text = Str_SN_Number;
                            if (Str_SN_Number == textBox_Send.Text)
                            {
                                richTextBox_ReceiveSN.BackColor = Color.LightGreen;
                            }
                        }));

                        break;

                        default:
                            break;

                    }

                
            }
            catch(Exception ex)
            {

            }
            


        }

        private void SerialPort1_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            Thread.Sleep(300);

            if (!serialPort.IsOpen) return;

            // Obtain the number of bytes waiting in the port's buffer
            int bytes = serialPort.BytesToRead;

            // Create a byte array buffer to hold the incoming data
            byte[] buffer = new byte[bytes];

            // Read the data from the port and store it in our buffer
            serialPort.Read(buffer, 0, bytes);

            if (buffer.Length > 0)
            {
                IncomingString = "[" + DateTime.Now.TimeOfDay.ToString().Substring(0, 11) + "]" + " Rx>" + "\n";

                List<byte[]> Massages = BreakIntoToMessages(buffer);

                foreach(byte[] message in Massages)
                {
                    ParseIncomingBuffer(message);
                }
                
            }


        }

        private List<byte[]> BreakIntoToMessages(byte[] i_Incomebuffer)
        {
            List<byte[]> ret = new List<byte[]>();
            int i = 0;
            while(i < i_Incomebuffer.Length - 3)
            {
                byte[] buf;
                if (i_Incomebuffer[i] == 0x7e & i_Incomebuffer[i+1] == 0x01 & i_Incomebuffer[i + 2] == 0xf1)
                {
                    int j = i;

                    while (i_Incomebuffer[j] != 0xae & j < i_Incomebuffer.Length -3)
                    {
                        j++;
                    }

                    buf = new byte[j - i + 1];

                    int y = 0;
                    for(int k = i; k <= j; k++)
                    {
                        buf[y] = i_Incomebuffer[k];
                        y++;
                    }



                    ret.Add(buf);
                    i += j;
                }
                else
                {
                    i++;
                }
                
            }

            return ret;
        }


        void textBox_SerialPort_addText(string i_str)
        {
            textBox_SerialPort.BeginInvoke(new EventHandler(delegate
            {
                textBox_SerialPort.AppendText(i_str + "\n\n");
                textBox_SerialPort.ScrollToCaret();
            }));
        }

        void scanComports()
        {
            cmbPortName.Items.Clear();

            string[] ports = SerialPort.GetPortNames();
            foreach (string port in ports)
            {
                cmbPortName.Items.Add(port);

            }
            cmbPortName.SelectedIndex = 0;
        }

        private void checkBox_ComportOpen_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_ComportOpen.Checked)
            {
                try
                {
                    checkBox_ComportOpen.BackColor = Color.Yellow;

                    //CloseSerialPortTimer = false;



                    // Set the port's settings

                    serialPort.BaudRate = int.Parse(cmbBaudRate.Text);

                    if (cmbBaudRate.Items.Contains(cmbBaudRate.Text) == false)
                    {
                        cmbBaudRate.Items.Add(cmbBaudRate.Text);
                    }

                    serialPort.DataBits = int.Parse(cmbDataBits.Text);
                    serialPort.StopBits = (StopBits)Enum.Parse(typeof(StopBits), cmbStopBits.Text);
                    serialPort.Parity = (Parity)Enum.Parse(typeof(Parity), cmbParity.Text);
                    serialPort.PortName = cmbPortName.Text;




                    serialPort.Open();

                    //ListenBox.Checked = false;
                    //groupBox_ServerSettings.Enabled = false;
                    //IsTimedOutTimerEnabled = false;


                    textBox_SerialPort_addText(" Serial port Opened with  " + " ,PortName = " + serialPort.PortName
                     + " ,BaudRate = " + serialPort.BaudRate +
                     " ,DataBits = " + serialPort.DataBits +
                     " ,StopBits = " + serialPort.StopBits +
                     " ,Parity = " + serialPort.Parity);
                    //SerialPortLogger.LogMessage(Color.Green, Color.LightGray,
                    // " Serial port Opened with  " + " ,PortName = " + serialPort.PortName
                    // + " ,BaudRate = " + serialPort.BaudRate +
                    // " ,DataBits = " + serialPort.DataBits +
                    // " ,StopBits = " + serialPort.StopBits +
                    // " ,Parity = " + serialPort.Parity,
                    // New_Line = true, Show_Time = true);

                    Timer100ms = 60;

                    checkBox_ComportOpen.BackColor = Color.LightGreen;


                    cmbBaudRate.Enabled = false;
                    cmbDataBits.Enabled = false;
                    cmbParity.Enabled = false;
                    cmbPortName.Enabled = false;
                    cmbStopBits.Enabled = false;

                    checkBox_ComportOpen.Text = " Close Port";


                }
                catch (Exception ex)
                {
                    checkBox_ComportOpen.Checked = false;

                    //SerialException = true;

                    textBox_SerialPort_addText( ex.Message.ToString());
                    return;
                }




            }
            else
            {

                //ComPortClosing = true;
                serialPort.Close();

                cmbBaudRate.Enabled = true;
                cmbDataBits.Enabled = true;
                cmbParity.Enabled = true;
                cmbPortName.Enabled = true;
                cmbStopBits.Enabled = true;

                checkBox_ComportOpen.BackColor = default(Color);
                checkBox_ComportOpen.Text = " Open Port";


                //groupBox_ServerSettings.Enabled = true;
            }
        }

        private void button_Clear_Click(object sender, EventArgs e)
        {
            textBox_SerialPort.Text = "";
        }

        private void button_ReScanComPort_Click(object sender, EventArgs e)
        {
            scanComports();
        }

        private void button_Send_Click(object sender, EventArgs e)
        {
            String tempStr = textBox_Send.Text; 

            byte[] BufferTemp = Encoding.ASCII.GetBytes(tempStr);

            byte[] bufferToSend =  BuildProtocolBuffer(0x0abf, BufferTemp);

            

            bool IsSent = SerialPortSendData(bufferToSend);

            if(IsSent == true)
            {
                ClearRxTextbox();
            }
        }

        bool SerialPortSendData(byte[] i_SendData)
        {
            try
            {
                if (serialPort.IsOpen)
                {
                    // Send the binary data out the port
                    serialPort.Write(i_SendData, 0, i_SendData.Length);

                    String Tempstr = ConvertByteArrayToString(i_SendData);

                   // textBox_SerialPort_addText("Tx:> [" + Tempstr + "]");



                    return true;

                }
            }
            catch (Exception ex)
            {
                textBox_SerialPort_addText(ex.Message.ToString());

            }


            return false;
        }


        void SendAsyncMassage(UInt32 i_data)
        {
            //AsyncMassageCounter++;

            byte[] buffer = new byte[4];
            buffer[3] = (byte)(i_data >> 24);
            buffer[2] = (byte)(i_data >> 16);
            buffer[1] = (byte)(i_data >> 8);
            buffer[0] = (byte)(i_data);
            byte[] bufferToSend = BuildProtocolBuffer(0x0a0c, buffer);
            SerialPortSendData(bufferToSend);

            //byte[] i_SendData = { 0x7e, 0xf1, 0x01, 0x0c, 0x0a, 0, 0, 0, 0, 0x7f, 0x11, 0x04, 0, 0x0b, 0, 0, 0, 0x97, 0, 0xae };
            //String PrintString = ConvertByteArrayToString(i_SendData);

            //textBox_SerialPort_addText(PrintString);

            //SerialPortSendData(i_SendData);

            //System.Threading.Thread.Sleep(10);

            //byte[] i_SendData2 = { 0x7e, 0xf1, 0x01, 0x0c, 0x0a, 0, 0, 0, 0, 0x80, 0x11, 0x04, 0, 0x0d, 0, 0, 0, 0x6e, 0, 0xae };
            //SerialPortSendData(i_SendData2);
        }

        int Timer100ms =0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            Timer100ms++;

            if(Timer100ms % 70 == 0 & serialPort.IsOpen)
            {

                SendAsyncMassage(0x0b);

                System.Threading.Thread.Sleep(10);

                SendAsyncMassage(0x0d);



            }
        }

        private static ushort ParseHexString(string hexNumber)
        {
            hexNumber = hexNumber.Replace("X", string.Empty);
            ushort result = 0;
            ushort.TryParse(hexNumber, System.Globalization.NumberStyles.HexNumber, null, out result);
            return result;
        }

        private String ConvertByteArrayToString(byte[] i_ByteArr)
        {
            String ret = "";
            int i = 0;
            foreach (byte by in i_ByteArr)
            {
                if (i == 0 || i == 1 || i == 2 || i == 3 || i == 5 || i == 9 || i == 11 || i == 13 || i == (i_ByteArr.Length - 3) || i == (i_ByteArr.Length - 1))
                {
                    ret += "|";
                }
                ret += by.ToString("X2") + " ";
                i++;

            }

            return ret;
        }

        //private void button1_Click(object sender, EventArgs e)
        //{
        //    byte[] buffer = Encoding.ASCII.GetBytes(textBox_data.Text);

        //    buffer[0] = 0;

        //    ushort Opcode = ParseHexString(textBox_Opcode.Text);

        //    byte[] ArrToSend = BuildProtocolBuffer(Opcode, buffer);

        //    String Sendstr = "";
        //    int i = 0;
        //    foreach(byte by in ArrToSend)
        //    {
        //        if(i == 0 || i==1 || i==2 || i == 3 || i == 5 || i == 9 || i == 11 || i == 13 || i == (ArrToSend.Length -3) || i == (ArrToSend.Length - 1))
        //        {
        //            Sendstr += "|";
        //        }
        //        Sendstr += by.ToString("X2") + " ";
        //        i++;
                
        //    }
        //    textBox_SerialPort_addText(Sendstr);
        //}

        //byte crc32b(byte[] crc_buff_in, UInt16 crc_buff_in_length)
        //{

        //    UInt16 i;
        //    byte crc;
        //    crc = 0xFF;

        //    for (i = 0; i < crc_buff_in_length; i++)
        //    {
        //        // simple xor of all bytes 
        //        crc = (byte)(crc ^ crc_buff_in[i]);
        //    }
        //    // return not of solution
        //    return (byte)~crc;
        //}
        int CurrentMessageTime = 0;
        byte[] BuildProtocolBuffer(ushort i_Opcode,byte[] i_data)
        {
            //
            //      [7E] [01] [F1] [01 00] [CC 78 64 20] [73 4A] [01 00] [00] [39 00] [AE]

            //7E 01 F1 01 00 87 C5 E1 89 C1 4C 01 00 00 57 00 AE
            //7E 01 F1 01 00 1C 8B 0F 8A C2 4C 01 00 00 6C 00 AE
            //7E 01 F1 01 00 AF 50 3D 8A C3 4C 01 00 00 37 00 AE
            //7E 01 F1 01 00 34 16 6B 8A C4 4C 01 00 00 BB 00 AE
            //7E 01 F1 01 00 C8 DB 98 8A C5 4C 01 00 00 78 00 AE
            //7E 01 F1 01 00 5D A1 C6 8A C6 4C 01 00 00 CA 00 AE
            //7E 01 F1 01 00 DE 66 F4 8A C7 4C 01 00 00 BD 00 AE


            byte[] sendArr = new byte[i_data.Length + 16];
            int i = 0;
            sendArr[i] = 0x7e;
            i++;

            sendArr[i] = 0x01;
            i++;

            sendArr[i] = 0xF1;
            i++;

            sendArr[i] = (byte)(i_Opcode);
            i++;

            sendArr[i] = (byte)(i_Opcode >> 8);
            i++;

            CurrentMessageTime++;
            sendArr[i] = (byte)(CurrentMessageTime );           //  7E 01 F1 01 00 5D A1 C6 8A C6 4C 01 00 00 CA 00 AE
            i++;

            sendArr[i] = (byte)(CurrentMessageTime >> 8);
            i++;

            sendArr[i] = (byte)(CurrentMessageTime >> 16);
            i++;

            sendArr[i] = (byte)(CurrentMessageTime >> 24);
            i++;


            //Reference counter
            sendArr[i] = 0;
            i++;
            sendArr[i] = 0;
            i++;

            //data length
            sendArr[i] = (byte)(i_data.Length );
            i++;
            sendArr[i] = (byte)(i_data.Length >> 8);
            i++;


            foreach (byte by in i_data)
            {
                sendArr[i] = by;
                i++;
            }

            int CRC_Length = sendArr.Length - 2;
            byte[] CRCArr = new byte[CRC_Length];
            int k = 0;
            for (int j=1; j < (CRC_Length); j++)
            {
                CRCArr[k] = sendArr[j];
                k++;
            }

            ushort CRC_result = crc32b(CRCArr, (ushort)(CRC_Length));
            sendArr[i] = (byte)(CRC_result );
            i++;

            sendArr[i] = (byte)(CRC_result >>  8);
            i++;

            sendArr[i] = (byte)0xAE;
            i++;

            return sendArr;


        }

        //void sendProtocolBytes(byte[] arr, byte cha)
        //{
        //    int i;

        //    byte[] sendArr = new byte[116];
        //    byte[] subArr = new byte[112];

        //    for (i = 0; i < 116; i++)
        //    {
        //        sendArr[i] = 0;
        //    }

        //    //Array.Copy(Qsend, subArr, 12);
        //    subArr[12] = cha;

        //    //Array.Copy(subArr, 0, testtest, 1, 112);

        //    ushort crcT;
        //    crcT = crc32b(subArr, 112);
        //    //crcT = Calc(crcTest);

        //    //textBox9.Text = crcT.ToString("X");

        //    //testtest[0] = 0x7e;
        //    //testtest[113] = (byte)crcT;
        //    //testtest[114] = (byte)(crcT >> 8);
        //    //testtest[115] = 0xae;

        //    //WriteData(testtest);

        //}



        private static ushort crc32b(byte[] crc_buff_in, ushort crc_buff_in_length)
        {

            int i;
            ushort crc;
            crc = 0xFFFF;

            for (i = 0; i < crc_buff_in_length; i++)
            {
                // simple xor of all bytes 
                crc ^= (ushort)crc_buff_in[i];
            }
            // return not of solution
            return (ushort)~crc;
        }

        private void textBox_Send_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void textBox_Send_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void textBox_Send_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        bool SN_Received = false;
        private void button_ReceiveSN_Click(object sender, EventArgs e)
        {
            byte[] buffer = new byte[4];
            buffer[3] = (byte)(0);
            buffer[2] = (byte)(0);
            buffer[1] = (byte)(0);
            buffer[0] = (byte)(0);
            byte[] bufferToSend = BuildProtocolBuffer(0x0abd, buffer);

            Timer100ms = 60;

            SN_Received = false;
            int CounterSend = 0;
            new Thread(() =>
            {
                while (SN_Received == false && CounterSend < 30)
                {
                
                        Thread.CurrentThread.IsBackground = true;
                        System.Threading.Thread.Sleep(100);
                        SerialPortSendData(bufferToSend);
                        CounterSend++;
                

                }
                SN_Received = false;
            }).Start();


        }

        private void textBox_SerialPort_KeyPress(object sender, KeyPressEventArgs e)
        {


            
        }

        private void textBox_SerialPort_KeyDown(object sender, KeyEventArgs e)
        {
            RichTextBox t = (RichTextBox)sender;
            if (e.KeyData == (Keys.C | Keys.Control))
            {
                t.Copy();
                e.Handled = true;
            }
            else if (e.KeyData == (Keys.X | Keys.Control))
            {
                t.Cut();
                e.Handled = true;
            }
            else if (e.KeyData == (Keys.V | Keys.Control))
            {
                t.Paste();
                e.Handled = true;
            }
            else if (e.KeyData == (Keys.A | Keys.Control))
            {
                t.SelectAll();
                e.Handled = true;
            }
            else if (e.KeyData == (Keys.Z | Keys.Control))
            {
                t.Undo();
                e.Handled = true;
            }
        }

        private void button_ClearRx_Click(object sender, EventArgs e)
        {
            ClearRxTextbox();
            ClearTxTextbox();

        }

        void ClearRxTextbox()
        {
            richTextBox_ReceiveSN.Text = "";
            richTextBox_ReceiveSN.BackColor = default(Color);
        }

        void ClearTxTextbox()
        {
            textBox_Send.Text = "";
            textBox_Send.BackColor = default(Color);
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
    }
